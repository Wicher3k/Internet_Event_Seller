class ChangeDataTypeForUserPrice < ActiveRecord::Migration[5.1]
  def change
      change_column :users, :money, :decimal, :default => 0, :null => false
  end
end
