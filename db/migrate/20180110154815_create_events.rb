class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :artist,  null: false
      t.integer :placesNumber,  null: false
      t.text :description,  null: false
      t.decimal :price_low,  null: false
      t.decimal :price_high,  null: false
      t.boolean :forAdult, default: false
      t.date :event_date,  null: false
      t.timestamps
    end
  end
end


