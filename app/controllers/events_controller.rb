
class EventsController < ApplicationController
  before_action :authenticate_user!, :only => [:new, :create,:edit] # sprawdzenie czy curent user jest zalogowany a potem czy jest adminem jak nie jest to redirect

  def index
    @events = Event.where("event_date >= :currentDate", {currentDate: Date.today} ).order(:event_date) #order by date
    @eventsOld = Event.where("event_date < :currentDate", {currentDate: Date.today} ).order(event_date: :desc) 
  end

  def new
      @event = Event.new
  end

  def create
    @myErrors=Array.new
    @ifError = false

    event_parmas = params.require(:event).permit(:artist, :placesNumber, :description, :price_low, :price_high, :forAdult, :photo, :event_date, :limitTicket)
    event_parmas[:placesNumberAll] = event_parmas[:placesNumber]
    @event = Event.new(event_parmas)
    puts   @event.inspect
    if event_parmas[:limitTicket].to_i > event_parmas[:placesNumber].to_i
      @myErrors.push([" ",'Invalid number of limit ticket'])
      @ifError = true
    end

    respond_to do |format|
      if @event.valid? && @ifError == false
        @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        for myError in @myErrors
          @event.errors.add( myError[0], myError[1])
        end
         # puts   @event.errors.messages.inspect
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end


  def show
    @event = Event.find(params[:id])
    
    #zarezerwowane bilety
    @ticketsReserved = Ticket.where(event_id: params[:id])
    @ticketReservedArray = Array.new
    for x in @ticketsReserved
      @ticketReservedArray.push(x.seat_id_seq)
    end

    #sprawdzenie czy juz mozna kupowac (mozna kupwoac 1 miesiac przed eventem)
    @canBuy = false
    if @event.event_date - 1.months < Date.today
      @canBuy = true
    end

    #zwiekszenie ceny jezeli jest grane w dniu dzisiejszym 
    if @event.event_date == Date.today && @event.price_high!=@event.price_low #rozwiazanie tymczasowe, mozna dodać w tabeli zmienna przehcowujaca zczy dokonana zostala zmina ceny, a najlepiej takie puścić crona raz w nocy zmieniajacy cene.
      @event.price_high = @event.price_high * 1.2
      @event.price_low =  @event.price_high
      @event.save()
    end
  end

  def edit
    if current_user.admin? # sprawdzenie czy current user jest adminem
      @event = Event.find(params[:id])
    else
      redirect_to "/events/index"
    end
  end

  def update
    if current_user.admin?
      respond_to do |format|
        event_parmas = params.require(:event).permit(:artist, :placesNumber, :description, :price_low, :price_high, :forAdult, :photo, :event_date, :limitTicket)
        @event = Event.find(params[:id])
        if @event.update(event_parmas)
          format.html { redirect_to @event, notice: 'Event was successfully edited.' }
          format.json { render :show, status: :ok, location: @event }
        else
          format.html { render :edit }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      end
      
    else
      redirect_to "/events/index"
    end 
  end

end
