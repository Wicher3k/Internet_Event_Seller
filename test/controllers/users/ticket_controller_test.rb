require 'test_helper'

class Users::TicketControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get users_ticket_show_url
    assert_response :success
  end

end
