class Users::TicketController < ApplicationController
  before_action :authenticate_user! 
  
  def show
    @tickets = Ticket.where(:user_id => current_user.id )
  end

  def wantdelete
    id_ticket= [params[:id]][0]
    @ticket = Ticket.find(id_ticket)
    @ticket.status = 1
    @ticket.price_return = (@ticket.price * 0.4 + ((@ticket.event.event_date.mjd - Date.today.mjd).to_f()/(@ticket.event.event_date.mjd - Date.today.mjd + 1))*0.5*@ticket.price ).round(2)
    @ticket.save
    redirect_to '/users/ticket/show', notice: 'Zgłoszono wniosek to zwrotu ticketu.'
  end

  def showTickets
    @name = User.find(params[:id])
    @tickets = Ticket.where(:user_id => params[:id])
  end
end
