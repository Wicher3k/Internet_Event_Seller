class AddPriceReturnToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :price_return, :decimal
  end
end
