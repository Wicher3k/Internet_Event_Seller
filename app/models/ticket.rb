class Ticket < ApplicationRecord
    belongs_to :event
    belongs_to :user
    validates :email_address, :presence => true
    validates :address, :presence => true
    validates :price, :presence => true, numericality: { greater_than: 0 }
    validates :seat_id_seq, :presence => true
end
