// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require bootstrap-sprockets
//= require bootstrap
//= require turbolinks
//= require_tree .

$(function(){ 

    // show ticket price
    $(document).on('change input','#ticket_price',function(){
        $('#priceShow').text($('#ticket_price').val())
    });

    // Date filter
    $(document).on('change','.inputDate',function(){
        var eventDate, dateFrom, dateTo;
        $('.columnEvent').show();

        dateFrom = $('.inputDate[name=dateFrom]').val();
        dateTo = $('.inputDate[name=dateTo]').val();
       
        if(dateFrom == ''){
            dateFrom = '2014-01-01';
        }
        if(dateTo == ''){
            dateTo = '2020-01-01';
        }
        dateTo = new Date(dateTo).getTime();
        dateFrom = new Date(dateFrom).getTime();
        $.each($('.columnEvent'), function( index, value ) {
            eventDate =new Date($( this ).attr("date")).getTime();
            if(eventDate<dateFrom || eventDate>dateTo ){
                $( this ).hide();
            }
        });
    });

    //seat Action

    $(document).on('click','.seat.empty',function(){
        var num,seq,seqid;
        seqid = $(this).text().replace(/ /g,'')
        num = +$("#numberOfTickets").val();
        seq = $('#ticket_seat_id_seq').val();

        if ($(this).hasClass('book')){
            $(this).removeClass('book');
            num -= 1;
            seq = seq.replace($(this).text(), "");    
            $(".ticket"+seqid).remove();

        }else{
            num += 1;
            $(this).addClass('book');
            seq += $(this).text()
            
            $('#buyTicket').append("<input type='hidden' name='seats[]' class='ticket"+seqid+"' value ='"+seqid+"' />")

        }
        $("#numberOfTickets").val(num)
        $('#ticket_seat_id_seq').val(seq)
    });


});

