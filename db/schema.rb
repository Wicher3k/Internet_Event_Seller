# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180121091719) do

  create_table "events", force: :cascade do |t|
    t.string "artist", null: false
    t.integer "placesNumber", null: false
    t.text "description", null: false
    t.decimal "price_low", null: false
    t.decimal "price_high", null: false
    t.boolean "forAdult", default: false
    t.date "event_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer "limitTicket"
    t.integer "placesNumberAll", default: 0, null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string "seat_id_seq", null: false
    t.text "address", null: false
    t.decimal "price", null: false
    t.string "email_address", null: false
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_id"
    t.integer "user_id"
    t.integer "status", default: 0
    t.decimal "price_return"
    t.index ["event_id"], name: "index_tickets_on_event_id"
    t.index ["user_id"], name: "index_tickets_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.boolean "admin", default: false
    t.datetime "remember_created_at"
    t.decimal "money", default: "0.0", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "adult", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
