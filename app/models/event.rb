class Event < ApplicationRecord

    has_many :tickets
    has_attached_file :photo
    validates_attachment :photo,
                     content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
    validates :artist, :presence => true
    validates :description, :presence => true
    validates :placesNumber, :presence => true
    validates :price_low, :presence => true, numericality: { greater_than: 0, less_than_or_equal_to: :price_high }
    validates :price_high, :presence => true, numericality: { greater_than: 0 }

    validate :event_date_not_from_past
       
    def event_date_not_from_past
        if  event_date.present?
            if  event_date < Date.today
                errors.add(:event_date, 'can\'t be from past.')
            end
        else
            errors.add(:event_date, 'can\'t be blank.')
        end
    end


    
end

