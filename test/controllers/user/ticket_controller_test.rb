require 'test_helper'

class User::TicketControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get user_ticket_show_url
    assert_response :success
  end

end
