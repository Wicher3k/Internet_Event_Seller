require 'test_helper'

class Users::MoneyControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get users_money_index_url
    assert_response :success
  end

  test "should get update" do
    get users_money_update_url
    assert_response :success
  end

end
