class AddLimitTicketsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :limitTicket, :integer, default: nil
  end
end
