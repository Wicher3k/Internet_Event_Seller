class AddAdultToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :adult, :boolean, default: false
  end
end
