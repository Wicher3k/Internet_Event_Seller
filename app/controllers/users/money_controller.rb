class Users::MoneyController < ApplicationController
  before_action :authenticate_user! 


  def index
    @user = User.find(current_user.id)
  end

  def update
    @user = User.find(current_user.id)
    @user_parmas = params.permit(:money, :moneyAdd)

    @user.money = @user_parmas[:moneyAdd].to_f + @user_parmas[:money].to_f
    respond_to do |format|
      if @user.save()
        format.html { redirect_to '/users/money/index', notice: 'Money Added.' }
        format.json { render :index, status: :ok, location: '/users/money/index' }
      else
        format.html { render :index }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
