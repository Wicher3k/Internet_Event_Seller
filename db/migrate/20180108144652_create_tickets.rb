class CreateTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :tickets do |t|
      t.string :seat_id_seq,  null: false
      t.text :address,  null: false
      t.decimal :price,  null: false
      t.string :email_address,  null: false
      t.string :phone

      t.timestamps
    end
  end
end
