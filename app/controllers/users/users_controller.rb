class Users::UsersController < ApplicationController
  before_action :authenticate_user! 

  def index
    if(current_user.try(:admin?))
      @users = User.all().order(:email)
    else
      redirect_to '/', alert: 'You have not permission!'
    end
  end

end
