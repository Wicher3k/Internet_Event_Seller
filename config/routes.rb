Rails.application.routes.draw do


  namespace :users do
    get 'index'  => 'users#index'
  end

  namespace :users do
    get 'money/index'
    post 'money/index' => 'money#update'
    post 'users' => 'registration#create'
    get 'ticket/show'
    get 'ticket/wantdelete/:id' => "ticket#wantdelete", :as => :ticket_wantdelete
    get 'showTickets/:id' => "ticket#showTickets", :as => :ticket_showTickets
    
  end

  get 'events/edit'
  patch 'events/:id' => 'events#update', :as => :events_update

  resources :events, :only => [:index, :new, :create, :show, :edit]

  resources :users, :only => [:index]

  resources :tickets
  
  devise_for :users , :controllers => { registrations: 'users/registrations' }, path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }

  post '/users' => 'users#create'

  


  root :to  => "events#index"
  # root :to  => "tickets#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
