class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /tickets
  # GET /tickets.json
  def index

    @tickets = Ticket.all
    @allTickets = true
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
    #czu może edytować bilety 
    if(@ticket.user_id == current_user.id || current_user.try(:admin?) )
      #zarezerwowane bilety 
      @ticketsReserved = Ticket.where(event_id: @ticket.event_id)
      @ticketReservedArray = Array.new
      for x in @ticketsReserved
        @ticketReservedArray.push(x.seat_id_seq)
      end
      @canBuy = true
    else
      redirect_to @ticket, alert: 'It\'s not your Ticket!'
    end
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @myErrors=Array.new
    @ifError = false
    @user = User.find(current_user.id)
    @event = Event.find(ticket_params[:event_id].to_i)
    @ticket = Ticket.new(:user_id => ticket_params[:user_id] , :seat_id_seq => "null" , :address => ticket_params[:address], :price => ticket_params[:price], :email_address => ticket_params[:email_address] , :phone =>  ticket_params[:phone], :event_id => ticket_params[:event_id])

    respond_to do |format|

      validation()

      if @ticket.valid? && @ifError == false

        for seatId in params[:seats] # many tickets
          @ticket = Ticket.new(:user_id => ticket_params[:user_id], :seat_id_seq => "null" , :address => ticket_params[:address], :price => ticket_params[:price], :email_address => ticket_params[:email_address] , :phone =>  ticket_params[:phone], :event_id => ticket_params[:event_id])
          @ticket.seat_id_seq=seatId
          @ticket.save
        end
        
        #odejmij pieniadze
        @user.money = @user.money.to_f - ticket_params[:price].to_f * ticket_params[:numberOfTickets].to_f
        @user.save()

        #odejmij liczbe ogolnych biletów
        reduceNumberOfTickets(@ticket.event_id, ticket_params[:numberOfTickets].to_i)
        
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        #zarezerwowane bilety 
        @ticketsReserved = Ticket.where(event_id: ticket_params[:event_id].to_i)
        @ticketReservedArray = Array.new
        for x in @ticketsReserved
          @ticketReservedArray.push(x.seat_id_seq)
        end

        #sprawdzenie czy juz mozna kupowac (mozna kupwoac 1 miesiac przed eventem)
        @canBuy = false
        if @event.event_date - 1.months < Date.today
          @canBuy = true
        end

        for myError in @myErrors
          @ticket.errors.add( myError[0], myError[1])
        end
        format.html { render 'events/show' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end

    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    @myErrors=Array.new
    @ifError = false
    @user = User.find(@ticket.user_id)
    @user.money = @user.money.to_f + @ticket.price.to_f
    @event = Event.find(ticket_params[:event_id].to_i)

    validation()
    
    if ticket_params[:numberOfTickets].to_i>1
      @myErrors.push([" ",'Select only one seat'])
      @ifError = true
    end

    respond_to do |format|
      if @ticket.valid? && @ifError == false
          @user.money = @user.money.to_f - ticket_params[:price].to_f
          @user.save()

          @ticket.update(:user_id => ticket_params[:user_id] , :seat_id_seq => params[:seats][0] , :address => ticket_params[:address], :price => ticket_params[:price], :email_address => ticket_params[:email_address] , :phone =>  ticket_params[:phone], :event_id => ticket_params[:event_id])
          format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
          format.json { render :show, status: :ok, location: @ticket }
      else
        @user.money = @user.money.to_f - @ticket.price.to_f
        @user.save()
        
        @ticketsReserved = Ticket.where(event_id: ticket_params[:event_id].to_i)
        @ticketReservedArray = Array.new
        for x in @ticketsReserved
          @ticketReservedArray.push(x.seat_id_seq)
        end

        #sprawdzenie czy juz mozna kupowac (mozna kupwoac 1 miesiac przed eventem)
        @canBuy = false
        if @event.event_date - 1.months < Date.today
          @canBuy = true
        end

        for myError in @myErrors
          @ticket.errors.add( myError[0], myError[1])
        end
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @user = User.find(@ticket.user_id)
    @user.money+=@ticket.price_return
    

    @event = Event.find(@ticket.event_id)
    @event.placesNumber+=1

    @ticket.destroy
    @user.save
    @event.save
    
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def reduceNumberOfTickets(event_id, number)
      @eventUpdate = Event.find(event_id)
      @eventUpdate.placesNumber -= number
      @eventUpdate.save()
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:user_id, :seat_id_seq, :address, :price, :email_address, :phone, :event_id, :numberOfTickets, :seats)
    end

    def validation
      @ifError = false
        #check money
      if @user.money.to_f - ticket_params[:price].to_f * ticket_params[:numberOfTickets].to_f  < 0
        @myErrors.push([" ",'You don\'t have enough money.'])
        @ifError = true
      end

      #check adult
      if !@user.adult
        if @event.forAdult == true
          @myErrors.push([" ",'You can not take part in this event (underage)'])
          @ifError = true
        end
      end

      #check price, but the price is checked before sending form
      if ticket_params[:price].to_f < @event.price_low || ticket_params[:price].to_f > @event.price_high
        @myErrors.push([:price,'Invalid Price'])
        @ifError = true
      end

      #check bought number of tickets
      if ticket_params[:numberOfTickets].to_i<1
        @myErrors.push([" ",'Choose your seat'])
        @ifError = true
      end


      #check number of tickets 0
      if @event.limitTicket != nil
        if ticket_params[:numberOfTickets].to_i + @user.tickets.where(event_id: ticket_params[:event_id].to_i ).count > @event.limitTicket
          @myErrors.push([" ",'You can\'t buy more then ' + @event.limitTicket.to_s + ' tickets'])
          @ifError = true
        end
      end

      return  @ifError

    end


end
