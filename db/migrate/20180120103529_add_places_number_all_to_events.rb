class AddPlacesNumberAllToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :placesNumberAll, :integer,  null: false, default:0
  end
end
